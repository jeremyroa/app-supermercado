import React, { Component } from 'react';
import Footer from './components/Footer'
import Router from './components/Router'
import Header from './components/Header'

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <Router />
        <Footer />
      </React.Fragment>
    );
  }
}

export default App;
