import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CardPersonal extends Component {
    
    borraPersonal = () => {
        this.props.borrarPersonal(this.props.personal._id)
    }

    render() {
        return (
            <div className=" col-12 col-md-4 my-3">
                <div className="card">
                    <div className="card-header">
                        Personal
                    </div>
                <div className="card-body ">
                    <h5 className="card-title">{this.props.personal.nombre}</h5>
                    <p className="card-text">Genero: <strong> {this.props.personal.genero} </strong> </p>
                    <p className="card-text">Edad: {this.props.personal.edad} </p>
                    <p className="card-text">Nacimiento: {this.props.personal.fnacimiento} </p>
                    <p className="card-text">Direccion: {this.props.personal.direccion} </p>
                    <p className="card-text">Cedula: {this.props.personal.cedula} </p>
                    <p className="card-text">Usuario: {this.props.personal.usuario} </p>
                    <p className="card-text">Email: {this.props.personal.email} </p>
                        <div className="btn-group" role="group" aria-label="Basic example">
                            <button onClick={this.borraPersonal} type="button" className="btn btn-danger">Eliminar</button>
                            <button type="button" disabled className="btn btn-info">Editar</button>
                            
                        </div>
                    </div>
                </div>
            </div>            
        );
    }
}

CardPersonal.propTypes = {
    personal: PropTypes.object.isRequired,
};

export default CardPersonal;