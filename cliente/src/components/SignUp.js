import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SignUp extends Component {
    
    //Nombre, cedula, edad, genero, fecha, correo, usuario, password, direccion
    
    nombreRef= React.createRef();
    cedulaRef= React.createRef();
    edadRef= React.createRef();
    generoRef= React.createRef();
    fechaRef= React.createRef();
    correoRef= React.createRef();
    usuarioRef= React.createRef();
    passwordRef= React.createRef();
    direccionRef= React.createRef();

    obtenerRegistro = (e) => {
        e.preventDefault()

        const nuevoUsuario = {
            nombre: this.nombreRef.current.value,
            cedula: this.cedulaRef.current.value,
            edad: this.edadRef.current.value,
            genero: this.generoRef.current.value,
            fnacimiento: this.fechaRef.current.value,
            email: this.correoRef.current.value,
            usuario: this.usuarioRef.current.value,
            contrasenia: this.passwordRef.current.value,
            direccion: this.direccionRef.current.value
        }
        this.props.recibirNuevoUsuario(nuevoUsuario)
        e.currentTarget.reset();
    }

    render() {
        return (
            <div className="card card-body mt-5 ml-3">
                <h3 className="card-body text-center">Registro</h3>
                <form onSubmit={this.obtenerRegistro}>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label htmlFor="usuario">Usuario</label>
                            <input ref={this.usuarioRef} type="text" className="form-control" id="usuario" 
                            placeholder="Usuario"/>
                        </div>
                        <div className="form-group col-md-6">
                            <label htmlFor="password">Password</label>
                            <input ref={this.passwordRef} type="password" className="form-control" id="password" placeholder="Password"/>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label htmlFor="name">Nombre</label>
                            <input ref={this.nombreRef} type="text" className="form-control" placeholder="Nombre" id="name"/>
                        </div>
                        <div className="form-group col-md-6">
                            <label htmlFor="cedula">Cedula</label>
                            <input ref={this.cedulaRef} type="text" className="form-control" placeholder="Cedula" id="cedula"/>
                        </div>
                    </div>
                    <div className="form-group">
                                <label htmlFor="correo">Correo</label>
                                <input ref={this.correoRef} type="email" className="form-control"placeholder="example@gmail.com" id="correo"/>
                    </div>
                    <div className="form-group form-row">
                            <div className="col-md-8">
                                <label htmlFor="fecha" className="col-form-label">Fecha</label>
                                <input ref={this.fechaRef} type="date" id="fecha" className="form-control" />
                            </div> 

                            <div className="col-md-4">
                                <label className="col-form-label">Genero</label>
                                <select ref={this.generoRef} id="genero" className="form-control">
                                    <option value="" defaultValue>Genero</option>
                                    <option value="MASCULINO" defaultValue>Masculino</option>
                                    <option value="FEMENINO" defaultValue>Femenino</option>
                                </select>
                            </div> 
                    </div>

                    <div className="form-row">
                        <div className="form-group col-2">
                            <label htmlFor="edad">Edad</label>
                            <input ref={this.edadRef} type="text" className="form-control" placeholder="Ej. 18" name="edad" maxLength="2"/>
                        </div>

                        <div className="form-group col-10">
                            <label htmlFor="direccion">Direccion</label>
                            <input ref={this.direccionRef} type="direccion" className="form-control" id="direccion" placeholder="Direccion"/>
                        </div>
                    </div>

                    <button type="submit" className="btn btn-primary">Registrar</button>
                </form>
            </div>
        );
    }
}

SignUp.propTypes = {
    recibirNuevoUsuario: PropTypes.func.isRequired
};

export default SignUp;