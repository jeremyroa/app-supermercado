import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CardCliente extends Component {

    borrarCliente = () => {
        this.props.borrarCliente(this.props.cliente._id)
    }
    render() {
        return (
            <div className=" col-12 col-md-4 my-3">
                <div className="card">
                    <div className="card-header">
                        Cliente
                    </div>
                <div className="card-body ">
                    <h5 className="card-title">{this.props.cliente.nombre}</h5>
                    <p className="card-text">Genero: <strong> {this.props.cliente.genero} </strong> </p>
                    <p className="card-text">Edad: {this.props.cliente.edad} </p>
                    <p className="card-text">Nacimiento: {this.props.cliente.fnacimiento} </p>
                    <p className="card-text">Direccion: {this.props.cliente.direccion} </p>
                    <p className="card-text">Cedula: {this.props.cliente.cedula} </p>
                        <div className="btn-group" role="group" aria-label="Basic example">
                            <button onClick={this.borrarCliente} type="button" className="btn btn-danger">Eliminar</button>
                            <button type="button" disabled className="btn btn-info">Editar</button>
                            
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

CardCliente.propTypes = {
    cliente: PropTypes.object.isRequired,
};

export default CardCliente;