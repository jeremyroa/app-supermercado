import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SignIn extends Component {
    
    usuarioRef = React.createRef();
    passwordRef = React.createRef();
    checkMeRef = React.createRef();
    

    ingresarUsuario = (e) => {
        
        
        e.preventDefault();
        
        let usuarioSingIn = {
            usuario: this.usuarioRef.current.value,
            contrasenia: this.passwordRef.current.value,
        }
        this.props.recibirUsuarioLogIn(usuarioSingIn)
        e.currentTarget.reset();
    }

    render() {
        return (
            <div className="card card-body mt-5 mr-3">
                <h3 className="card-body text-center">Ingreso</h3>
                <form onSubmit={this.ingresarUsuario}>
                    <div className="form-group">
                        <label htmlFor="usuario">Usuario</label>
                        <input ref={this.usuarioRef} type="text" className="form-control" id="usuarioIn"
                         placeholder="Ej. usuario"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input ref={this.passwordRef} type="password" className="form-control" id="passwordIn" placeholder="Ej. Password"/>
                    </div>
                    <button type="submit" className="btn btn-primary">Ingresa</button>
                </form>
            </div>
        );
    }
}

SignIn.propTypes = {
    recibirUsuarioLogIn: PropTypes.func.isRequired
};

export default SignIn;