import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ModalCliente extends Component {
    
    nombreClienteRef = React.createRef()
    cedulaClienteRef = React.createRef()
    edadClienteRef = React.createRef()
    generoClienteRef = React.createRef()
    direccionClienteRef = React.createRef()
    fnacimientoClienteRef = React.createRef()

    nuevoCliente = (e) => {
        e.preventDefault()

        const  nuevoCliente = {
           nombre: this.nombreClienteRef.current.value,
           cedula: this.cedulaClienteRef.current.value,
           edad: this.edadClienteRef.current.value,
           genero: this.generoClienteRef.current.value,
           direccion: this.direccionClienteRef.current.value,
           fnacimiento: this.fnacimientoClienteRef.current.value
        }
        this.props.agregarCliente(nuevoCliente)

        e.currentTarget.reset()
    }

    render() {
        return (
            <div className="modal fade" id="modalcliente" tabIndex="-1">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="nuevoCliente">Nuevo Cliente</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form onSubmit={this.nuevoCliente}>
                    <div className="modal-body">
                        <div className="form-group">
                            <label htmlFor="nombreCliente" className="col-form-label">Nombre:</label>
                            <input ref={this.nombreClienteRef} type="text" className="form-control" id="nombreCliente"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="edadCliente" className="col-form-label">Edad</label>
                            <input ref={this.edadClienteRef} type="text" className="form-control" id="edadCliente"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="fechaCliente" className="col-form-label">Fecha</label>
                            <input ref={this.fnacimientoClienteRef} type="date" className="form-control" id="fechaCliente"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="direccionCliente" className="col-form-label">Dirección</label>
                            <input ref={this.direccionClienteRef} type="text" className="form-control" id="direccionCliente"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="cedulaCliente" className="col-form-label">Cedula</label>
                            <input ref={this.cedulaClienteRef} type="text" className="form-control" id="cedulaCliente"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="generoCliente" className="col-form-label">Genero</label>
                            <select ref={this.generoClienteRef} id="generoCliente" className="form-control"> 
                                <option value="" defaultValue>Elige</option>
                                <option value="MASCULINO" defaultValue>Masculino</option>
                                <option value="FEMENINO" defaultValue>Femenino</option>      
                            </select>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" className="btn btn-primary">Agregar Cliente</button>
                    </div>
                </form>
                </div>
            </div>
            </div>
        );
    }
}

ModalCliente.propTypes = {
    agregarCliente: PropTypes.func.isRequired
};

export default ModalCliente;