import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CardProducto extends Component {
    borrarProducto = () => {
        console.log('ESTA EN BORRAR PRODUCTO')
        this.props.borrarProducto(this.props.producto._id)
    }
    render() {
        return (
            <div className=" col-12 col-md-4 my-3">
                <div className="card">
            <div className="card-header">
                Producto
            </div>
                <div className="card-body ">
                    <h5 className="card-title">{this.props.producto.nombre}</h5>
                    <p className="card-text">Categoria: <strong> {this.props.producto.categoria} </strong> </p>
                    <p className="card-text">Precio: {this.props.producto.precio} </p>
                        <div className="btn-group" role="group" aria-label="Basic example">
                            <button onClick={this.borrarProducto} type="button" className="btn btn-danger">Eliminar</button>
                            <button type="button" disabled className="btn btn-info">Editar</button>
                            
                        </div>
                    </div>
                </div>
            </div>
            
        );
    }
}

CardProducto.propTypes = {
    producto: PropTypes.object.isRequired,
};

export default CardProducto;