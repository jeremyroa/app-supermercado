import React from 'react';

const Header = props => {
    return (
        <div className="bg-primary sticky-top">
            <nav className="navbar navbar-expand-lg navbar-dark container">
                <a className="navbar-brand m-auto" href="/">MyMarket</a>
           </nav>
        </div>
    );
};


export default Header;