import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SignIn from './SignIn'
import SignUp from './SignUp'

class Login extends Component {
    

    enviarRouterEstadoLogin = (user) => {
        this.setState({
            resultadoSignUp: user,
            estadoLogin: user.ok
        })
        user.ok ? alert('Ingreso con exito') : alert('Usuario o contraseña invalida')
        this.props.ingresarPpal(this.state.estadoLogin,user)
    }

    state = {
        login: {},
        registro: {},
        resultadoLogIn: {},
        resultadoSignUp: {},
        estadoLogin: '',
        estadoRegistro: ''
    }
    componentDidUpdate(prevProps, prevState){
        if(prevState.login !== this.state.login){
          this.comprobarLogin(this.state.login);
        }
        if(prevState.registro !== this.state.registro){
          this.postPersonal();
        }
    }

    postPersonal = () => {

        let url = 'http://localhost:8080/personal'
        const options = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(this.state.registro)
        }
        
        fetch(url,options)
        .then(res => res.json())
        .then(user => {
            alert('Registro con Exito')
            this.setState({
                resultadoSignUp: user,
                estadoRegistro: true
            })
        })
        .catch(err => {
            alert('Registro Fallido')
            this.setState({
                resultadoSignUp: err,
                estadoRegistro: false
            })
        })

    }  
    comprobarLogin = (usuarioForm) => {
        let url = 'http://localhost:8080/login'
        const options = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(usuarioForm)
        }
    
        fetch(url,options)
        .then(res => res.json())
        .then(user => this.enviarRouterEstadoLogin(user))
        .catch(err => {
            alert('Usuario o Contraseña Invalida')
            this.setState({
                resultadoLogIn: err,
                estadoLogin: false
            })
        })  
    }

    recibirUsuarioLogIn = (usuarioForm) => {
        if(usuarioForm.usuario === '' || usuarioForm.contrasenia === ''){
            this.setState({
              estadoLogin: true
            })
          }else{
            this.setState({
              estadoLogin: false,
              login: usuarioForm
            })
        }
    }

    recibirNuevoUsuario = (usuarioNuevo) => {
        console.log(usuarioNuevo)
        
        for(const valor in usuarioNuevo){
            if(valor === ''){
                
                this.setState({
                    estadoRegistro: true
                })
                return
            }
        }
        this.setState({
            estadoRegistro: false,
            registro: usuarioNuevo
        })
    }

    render() {
        // const {cod} = this.state.login
        // if(cod === '400'){
        //     console.log('error')
        // }

        return (
            <div className="container my-5">
                <div className="mt-4">
                    <h2 className="text-center">¡Crea tu usuario o Registrate ahora!</h2>
                </div>
                <div className="row">
                    <div className="col-12 col-lg-6">
                        <SignIn 
                        recibirUsuarioLogIn = {this.recibirUsuarioLogIn}
                        />
                    </div>
                    <div className="col-12 col-lg-6">
                            <SignUp 
                                recibirNuevoUsuario = {this.recibirNuevoUsuario}
                            />
                    </div>
                </div>
            </div>
        );
    }
}

Login.propTypes = {
    ingresarPpal: PropTypes.func.isRequired
};

export default Login;