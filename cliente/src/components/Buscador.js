import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Buscador extends Component {

    coleccionRef = React.createRef();
    minimoRef = React.createRef();
    limiteRef = React.createRef();

    obtenerColeccion = (e) => {
        e.preventDefault()

        const coleccionParams = {
            coleccion: this.coleccionRef.current.value,
            minimo: this.minimoRef.current.value,
            limite: this.limiteRef.current.value
        }

        this.props.buscarColeccion(coleccionParams)

    }

    render() {
        return (
            <div className="container">
                <form onSubmit={this.obtenerColeccion}>
                    <div className="form-row mx-5 my-5">
                        <div className="col-lg-3 pr-2">
                        <select ref={this.coleccionRef} id="coleccion" className="form-control"> 
                                <option value="" defaultValue>Coleccion</option>
                                <option value="cliente" defaultValue>Cliente</option>
                                <option value="personal" defaultValue>Personal</option>
                                <option value="producto" defaultValue>Producto</option>
                                
                        </select>
                        </div>
                        <div className="col-lg-3 pr-2">
                            <input type="text" ref={this.minimoRef} className="form-control" placeholder="Minimo" name="min" maxLength="2"/>
                        </div>

                        <div className="col-lg-3 pr-2">                        
                            <input  type="text" ref={this.limiteRef} className="form-control" placeholder="Limite" name="limite" maxLength="2"/>
                        </div>

                        <div className="col-lg-3">
                            <input type="submit" className="btn btn-primary" value="Buscar"/>
                        </div>

                    </div>
                </form>
            </div>
        );
    }
}

Buscador.propTypes = {
    buscarColeccion: PropTypes.func.isRequired
};

export default Buscador;