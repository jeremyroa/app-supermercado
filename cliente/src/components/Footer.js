import React from 'react';

const Footer = props => {
    return (
        <footer className="bg-dark mt-5 text-white py-2 text-center">
            <p>&copy; {(new Date()).getFullYear()}
                <span className="ml-3 text-white-50">Designed by Jeremy Roa
                </span>
            </p>
        </footer>
    );
};

export default Footer;