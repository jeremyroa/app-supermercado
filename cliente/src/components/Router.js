import React, { Component } from 'react'
import { BrowserRouter,Route, Switch } from 'react-router-dom'

import Login from './Login'
import Principal from './Principal';

class Router extends Component {
    state = {
        estadoLogin: '',
        usuarioPpal: {}
    }

    componentDidMount(){
        this.setState({
            estadoLogin: false
        })
    }
    
    ingresarPpal = (estadoLogin,usuarioLogin) => {
        this.setState({
            estadoLogin,
            usuarioPpal: usuarioLogin
        })
    }



    render() {
        let estado 
        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/" render={() => this.state.estadoLogin ? <Principal/> : <Login 
                                                                                        ingresarPpal= {this.ingresarPpal}
                                                                                        />}/>
                </Switch>
            </BrowserRouter>
        );
    }
}



export default Router;