import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ModalProducto extends Component {
    
    nombreProductoRef = React.createRef()
    precioProductoRef = React.createRef()
    cantidadProductoRef = React.createRef()
    categoriaProductoRef = React.createRef()
    
    nuevoProducto = (e) => {
        e.preventDefault()

        const  nuevoProducto = {
            nombre: this.nombreProductoRef.current.value,
            precio: this.precioProductoRef.current.value,
            cantidad: this.cantidadProductoRef.current.value,
            categoria: this.categoriaProductoRef.current.value
        }

        this.props.agregarProducto(nuevoProducto)
        e.currentTarget.reset();
    }

    render() {
        return (
            <div className="modal fade" id="modalproducto" tabIndex="-1">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="nuevoProducto">Nuevo Producto</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form onSubmit={this.nuevoProducto}>
                    <div className="modal-body">
                        <div className="form-group">
                            <label htmlFor="nombreProducto" className="col-form-label">Nombre:</label>
                            <input ref={this.nombreProductoRef} type="text" className="form-control" id="nombreProducto"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="precioProducto" className="col-form-label">Precio</label>
                            <input ref={this.precioProductoRef} type="text" className="form-control" id="precioProducto"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="cantidadProducto" className="col-form-label">Cantidad</label>
                            <input ref={this.cantidadProductoRef} type="text" className="form-control" id="cantidadProducto"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="categoriaProducto" className="col-form-label">Categoria</label>
                            <select ref={this.categoriaProductoRef} id="categoriaProducto" className="form-control"> 
                                <option value="" defaultValue>Elige</option>
                                <option value="CARNE" defaultValue>Carne</option>
                                <option value="VEGETALES" defaultValue>Vegetales</option>    
                                <option value="FRUTAS" defaultValue>Frutas</option>    
                                <option value="EMBUTIDOS" defaultValue>Embutidos</option>    
                            </select>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" className="btn btn-primary">Agregar Producto</button>
                    </div>
                </form>
                </div>
            </div>
            </div>
        );
    }
}

ModalProducto.propTypes = {
    agregarProducto: PropTypes.func.isRequired
};

export default ModalProducto;