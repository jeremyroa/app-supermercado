import React, { Component } from 'react';
import Buscador from './Buscador'
import CardProducto from './CardProducto';
import CardCliente from './CardCliente'
import CardPersonal from './CardPersonal'
import ModalProducto from './ModalProducto';
import ModalCliente from './ModalCliente'

class Principal extends Component {
    state = {
        error: '',
        productos: [],
        clientes: [],
        personal: [],
        agregar: ''
    }
    componentDidMount() {
        this.setState({
            error: false
        })
    }

    buscarColeccion = (coleccion) => {

        if(coleccion.coleccion === '' || isNaN(coleccion.minimo) || isNaN(coleccion.limite)){
            this.setState({
                error: true
            })
            console.log('hubo error')
        }
        else {
        let url = `http://localhost:8080/${coleccion.coleccion}?desde=${coleccion.minimo || 0}&limite=${coleccion.limite || 5}`

        fetch(url)
            .then(res => res.json())
            .then(cols => this.controlColeccion(cols,coleccion.coleccion))
            .catch(err => console.log(err))
        }

        

    }
    controlColeccion = (coleccionDB,coleccionSearching) => {
        console.log(coleccionDB,coleccionSearching)
        switch (coleccionSearching) {
            case 'producto':
                this.setState({
                    error: false,
                    productos: coleccionDB.productos,
                    clientes: [],
                    personal: []
                })
                break;
            case 'cliente':
                this.setState({
                    error: false,
                    productos: [],
                    clientes: coleccionDB.clientes,
                    personal: []
                })
                
                break;
            case 'personal':
                this.setState({
                    error: false,
                    productos: [],
                    clientes: [],
                    personal: coleccionDB.personal
                })
                break;
        
            default:
                break;
        }
    }
    borrarProducto = (id) => {
        let url = `http://localhost:8080/producto/${id}`
        const options = {
          method: 'DELETE',
        }
        console.log(id)
        fetch(url,options)
            .then(res => res.json())
            .then(cl => {
                alert('Refresque la busqueda')
                this.setState({
                productos: (this.state.productos.filter(a => cl.producto._id !== a._id)) 
            })
        })
            .catch(err => console.log(err))
    }

    borrarCliente = (id) => {
        let url = `http://localhost:8080/cliente/${id}`
        const options = {
          method: 'DELETE',
        }

        fetch(url,options)
            .then(res => res.json())
            .then(cl => this.setState({
                clientes: (this.state.clientes.filter(a => cl.cliente._id !== a._id)) 
            }))
            .catch(err => console.log(err))

    }

    borrarPersonal = (id) => {

        let url = `http://localhost:8080/personal/${id}`
        const options = {
          method: 'DELETE',
        }

        fetch(url,options)
            .then(res => res.json())
            .then(cl => this.setState({
                personal: (this.state.personal.filter(a => cl.personal._id !== a._id)) 
            }))
            .catch(err => console.log(err))        
    }

    cambiarCartas = () => {
        if(!this.state.error && this.state.productos.length > 0){
           const productos = this.state.productos
            return (
                productos.map(producto => (
                    <CardProducto 
                        key = {producto._id}
                        producto = {producto}
                        borrarProducto = {this.borrarProducto}
                    />
                ))
           ) 
        }
        else if(!this.state.error && this.state.clientes.length > 0){
            const  cliente = this.state.clientes
            console.log(cliente[0]._id)
            return (
                cliente.map(cliente => (
                    <CardCliente
                        key = {cliente._id}
                        cliente = {cliente}
                        borrarCliente = {this.borrarCliente}
                    />
                ))
           ) 
        }else {
            const  personals = this.state.personal
            return (
                personals.map(personal => (
                    <CardPersonal 
                        key = {personal._id}
                        personal = {personal}
                        borrarPersonal = {this.borrarPersonal}
                    />
                ))
           ) 
        }

    }
    render() {
        return (
            <div className="container text-center mt-5">
                <h1 className="h3">Editar Base de Datos por Colección</h1>
                <Buscador 
                    buscarColeccion = {this.buscarColeccion}
                />
                <div className="text-center mx-5 px-5 mb-4">
                        <button type="button"data-toggle="modal" data-target="#modalcliente" className="btn btn-outline-primary btn-lg">Agregar Cliente</button>
                        <button type="button" data-toggle="modal" data-target="#modalproducto"className="btn btn-outline-secondary btn-lg">Agregar Producto</button>

                </div>
                
                <div className="row">
                    {this.cambiarCartas()}
                </div>
                <div>
                    <ModalProducto 
                        agregarProducto = {this.agregarProducto}
                    />
                </div>
                <div>
                    <ModalCliente 
                        agregarCliente = {this.agregarCliente}
                    />
                </div>
            </div>
        );
    }

    agregarProducto = (productoNuevo) => {
        let url = 'http://localhost:8080/producto'
        const options = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(productoNuevo)
        }
        
        fetch(url,options)
        .then(res => res.json())
        .then(user => {
            user.ok ? alert('Registro con Exito') : alert('Registro sin exito')
        })
        .catch(err => {
            alert('Registro Fallido')
            console.log(err)
        })
    }

    agregarCliente = (nuevoCliente) => {

        let url = 'http://localhost:8080/cliente'
        const options = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(nuevoCliente)
        }
        
        fetch(url,options)
        .then(res => res.json())
        .then(user => {
            user.ok ? alert('Registro con Exito') : alert('Registro sin exito')
        })
        .catch(err => {
            alert('Registro Fallido')
            console.log(err)
        })
    }
}


export default Principal;