/**
 * @author Jeremy Roa
 * @date 10/12/2018
 * @description CRUD Supermercado
 */
const express = require('express');
const Producto = require('../models/producto');
const app = express();
const _ = require('underscore');


app.get('/producto', (req, res) => {

    let desde = req.query.desde || 0;
    desde = Number(desde);

    let limite = req.query.limite || 5;
    limite = Number(limite);
    if (isNaN(desde) || isNaN(limite)) {
        return res.status(400)
            .json({
                ok: false,
                message: 'No es un número el valor ingresado'
            })
    }
    Producto.find({estado: true})
        .skip(desde)
        .limit(limite)
        .exec((err, productos) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            Producto.count({
                estado: true
            }, (err, conteo) => {

                res.json({
                    ok: true,
                    productos,
                    cuantos: conteo
                });

            });


        });

})
app.post('/producto', (req, res) => {

    let body = req.body

    if (body.cantidad < 1 || body.precio < 1 || isNaN(body.precio) || isNaN(body.cantidad)) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'La cantidad y/o el precio no puede ser menor a 1 y debe ser un numero'
            }
        })
    }

    let producto = new Producto({
        nombre: body.nombre,
        cantidad: body.cantidad,
        categoria: body.categoria,
        precio: body.precio,
    })

    producto.save((err, productoDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            producto: productoDB
        });
    })


})
app.put('/producto/:id', (req, res) => {

    let id = req.params.id;
    let body = _.pick(req.body, ['nombre', 'categoria', 'cantidad', 'precio','estado']);
    
    let nanCantidad;
    let nanPrecio;

    if(body.precio !== undefined){
        nanPrecio = isNaN(Number(body.precio))
    }
    else{
        nanPrecio = false
    }

   
    if(body.cantidad !== undefined)
        nanCantidad = isNaN(Number(body.cantidad))

    else
        nanCantidad = false


    if (Number(body.cantidad )< 1 || Number(body.precio) < 1 || nanCantidad || nanPrecio) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'La cantidad y/o el precio no puede ser menor a 1 y debe ser un numero'
            }
        })
    }

    Producto.findByIdAndUpdate(id, body, {context: 'query',
        context: 'query',
        new: true,
        runValidators: true
    }, (err, productoDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }



        res.json({
            ok: true,
            producto: productoDB
        });

    })

})
app.delete('/producto/:id', (req, res) => {

    let id = req.params.id;

    let cambiaEstado = {
        estado: false
    };

    Producto.findByIdAndUpdate(id, cambiaEstado, {
        new: true,
        runValidators: true
    }, function (err, productoBorrar) {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            })
        }
        res.json({
            ok: true,
            usuario: productoBorrar
        })


    })


})

module.exports = app;