/**
 * @author Jeremy Roa
 * @date 10/12/2018
 * @description CRUD Supermercado
 */

const express = require('express');

const app = express();

app.use(require('./producto'));
app.use(require('./personal'));
app.use(require('./cliente'));
app.use(require('./login'));

module.exports = app;