/**
 * @author Jeremy Roa
 * @date 10/12/2018
 * @description CRUD Supermercado
 */
const express = require('express');

const bcrypt = require('bcrypt');
const _ = require('underscore');
const Personal = require('../models/personal');

const app = express();
const validator = require('validator')


app.get('/personal', (req, res) => {

    let desde = req.query.desde || 0;
    desde = Number(desde);

    let limite = req.query.limite || 5;
    limite = Number(limite);
    if (isNaN(desde) || isNaN(limite)) {
        return res.status(400)
            .json({
                ok: false,
                message: 'No es un número el valor ingresado'
            })
    }
    Personal.find({})
        .skip(desde)
        .limit(limite)
        .exec((err, personal) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            Personal.count({}, (err, conteo) => {

                res.json({
                    ok: true,
                    personal,
                    cuantos: conteo
                });

            });


        });

})
app.post('/personal', (req, res) => {
    let body = req.body
    body.cedula = Number(body.cedula)
    body.edad = Number(body.edad)
    if (body.cedula < 1 || body.edad < 1 || !validator.isEmail(body.email)) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'Edad, email y/o cedula incorrecta'
            }
        })
    }

    let personal = new Personal({
        nombre: body.nombre,
        edad: body.edad,
        fnacimiento: body.fnacimiento,
        direccion: body.direccion,
        genero: body.genero,
        cedula: body.cedula,
        usuario: body.usuario,
        contrasenia: bcrypt.hashSync(body.contrasenia, 10),
        email: body.email
    })

    personal.save((err, personalDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            personal: personalDB
        });
    })

})
app.put('/personal/:id', (req, res) => {

    let id = req.params.id;
    let body = _.pick(req.body, ['nombre', 'cedula', 'edad', 'fnacimiento', 'genero', 'direccion', 'email', 'usuario']);
    let validarEmail;
    
    if(body.email !== undefined)
        validarEmail = !validator.isEmail(body.email)
    else
        validarEmail = false

    if (Number(body.cedula) < 1 || Number(body.edad) < 1 || validarEmail) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'Edad, email y/o cedula incorrecta'
            }
        })
    }

    Personal.findByIdAndUpdate(id, body, {
        context: 'query',
        new: true,
        runValidators: true
    }, (err, personalDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }



        res.json({
            ok: true,
            personal: personalDB
        });
    })

})
app.delete('/personal/:id', (req, res) => {
    let id = req.params.id
    Personal.findByIdAndRemove(id, (err, personalBorrar) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                error: err
            })
        }
        if (!personalBorrar) {
            return res.status(400).json({
                ok: false,
                error: {
                    message: 'El Personal ya se habia borrado antes'
                }
            })
        }

        res.json({
            ok: true,
            personal: personalBorrar
        })
    })
})

module.exports = app;