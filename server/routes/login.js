const express = require('express');

const bcrypt = require('bcrypt');
const _ = require('underscore');

const Personal = require('../models/personal');

const app = express();
const validator = require('validator')

app.get('/login',(req,res) => {
    res.json({
        ok:true
    })
})

app.post('/login',(req,res) => {
    
    let body = req.body
    if(body.usuario === undefined || body.contrasenia === undefined){
        return res.status(400).json({
            ok: false,
            err: {
                message: 'Campos vacios'
            }
        })
    }

    Personal.findOne({usuario: body.usuario},(err,usuarioDB) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                })
            }
            if(!usuarioDB) {
                return res.status(400).json({
                    ok: false,
                    error: {
                        message: "Usuario o contraseña invalida"
                    }
                })
            }
            if( ! bcrypt.compareSync(body.contrasenia,usuarioDB.contrasenia)) {
                return res.status(400).json({
                    ok: false,
                    error: {
                        message: "Usuario o contraseña invalida"
                    }
                })
            }
    

            res.json({
                ok: true,
                personal: usuarioDB
            })
    })
})

module.exports = app;